module gitlab.com/hugo-blog/hugo-theme-7e5

// uncomment the following lines for development:
// replace gitlab.com/hugo-blog/hugo-module-gdpr-privacy => ../hugo-module-gdpr-privacy
// replace gitlab.com/hugo-blog/hugo-module-meta => ../hugo-module-meta

go 1.19

require gitlab.com/hugo-blog/hugo-module-gdpr-privacy v0.0.5 // indirect
require gitlab.com/hugo-blog/hugo-module-meta v0.0.3 // indirect
